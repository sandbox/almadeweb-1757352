Description
============
Yet another Q&A stackoverflow.com clone.

This module defines two content types, questions and answers related to questions. 
It defines views and page displays to work with them.
It has a serie of submodules add:
- Best answer selection (flag) for its question
- Voting of questions and answers
- Integration with userpoints trough rules, to add points to users on voting and 
on flagging an answer as best answer.  

TODOs
============
- QAS profile
- Ajax submit on answer creation
- Official comments for questions and answers
  - Ajax for coments creation
- flag delete, abuse, bookmark(favorites) with views
- User questions and answer views
- Translations
- Tests
  - on_question_delete_test
- redirect on node/add/qas-answer

TODOs depending on other modules
============
- export flag_view_refresh bug
- votingAPI views_field_handler bug
